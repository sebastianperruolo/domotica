# Trabajo final del Taller de Microcontroladores

## Introducción

El proyecto contiene la información y archivos necesarios para implementar una alarma hogareña sencilla y tener un timbre con notificaciones PUSH.

Un servidor MQTT unirá la alarma, el timbre y un servidor con Home Assistant.

![MQTT Broker con conexiones a Home Assistant, timbre y alarma](docs/imagenes/MQTT Broker.svg "Conexiones con MQTT")

## Alarma

La alarma está controlada por un Arduino Nano con:

* Una entrada de `teclado numérico` para desactivar la alarma físicamente.
* Una entrada de `sensor de movimiento`.
* Una conexión I2C con un ESP8266 para integrarse con MQTT.

### Casos de uso

1. Al ingresar el pin en el `teclado numérico` cambia el estado (_on_/_off_) de la `alarma`.
2. Un mensaje MQTT cambia el estado de la `alarma`.
3. Un mensaje MQTT consulta el estado de la `alarma`.
4. Si la `alarma` está en estado _on_, un `sensor de movimiento` puede _dispararla_.
5. Si la `alarma` está en estado _on_, un mensaje MQTT puede _dispararla_.
6. Si la `alarma` se _dispara_ se envía un mensaje MQTT.

## Timbre

El timbre está controlado por el ESP8266 con:

* Una salida de `buzzer`.
* Una entrada de `pulsador`.
* Una conexión I2C con un Arduino Nano para permitirle integrarse con MQTT.

### Casos de uso

1. Un `pulsador` _dispara_ el `timbre`.
2. Si el `timbre` se _dispara_ activa el `buzzer` y envía un mensaje MQTT.
3. Un mensaje MQTT activa el `buzzer`.

## Prototipo

![Diseño del Prototipo](docs/imagenes/Prototipo.png "Prototipo")

## Home Assistant

La idea es agregar un servidor con `Home Assistant` configurado para acceder al broker MQTT.

Si llega un mensaje MQTT de `alarma` _disparada_:
a. Se envía otro mensaje MQTT para activar el `buzzer` del `timbre`.
b. Se envía una notificación PUSH al celular.

Si llega un mensaje MQTT de `timbre` _disparado_:
a. Se envía una notificación PUSH al celular.

Además `Home Assistant`:
a. Muestra el estado de la `alarma` consultandolo por MQTT.
b. Permite cambiar el estado la `alarma` usando MQTT.

Será conveniente usar un servicio de DNS dinámico[^1] para poder acceder a `Home Assistant` desde fuera del hogar. Además será necesario utilizar [Let’s Encrypt](https://letsencrypt.org/)

[^1]: La idea es usar [No-IP](https://www.noip.com/).

# Links útiles

* [Use ARDUINO Nano as I2C Slave IO Digital and analog expander for ESP8266](https://github.com/MajorLee95/nanoI2CIOExpander)
* [How to Set Up a Keypad on an Arduino](http://www.circuitbasics.com/how-to-set-up-a-keypad-on-an-arduino/)
* [PIR Motion Sensor: How to Use PIRs w/ Arduino & Raspberry Pi](https://create.arduino.cc/projecthub/electropeak/pir-motion-sensor-how-to-use-pirs-w-arduino-raspberry-pi-18d7fa)
