// 52

void setup() {
  // put your setup code here, to run once:
  Wire.begin(1);
  Wire.onReceive(receiveEvent);
  Serial.begin(9600);
}

void loop() {
  delay(300);
}

void receiveEvent(int numBytes) {
  String text = "";
  while (Wire.available() > 0) {
    text = text + String(Wire.read());
  }
  Serial.println("Llego " + text);
}
