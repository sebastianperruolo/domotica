#include <SoftwareSerial.h>

int led = 13;
char idMaquina = '2';
char idDestino = '0';

SoftwareSerial serial(10,11);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  serial.begin(9600);
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);
}

String m;
long tiempo = 0;
bool contestar = false;
String mensaje;
int tiempoDelay = 1000;

void loop() {
  //Comando inicial
  if (Serial.available() > 0){
    m = Serial.readStringUntil('\n');
    m = idMaquina + m;
    serial.println(m);
    Serial.println("Mensaje enviado");
  }

  //Contestación
  if (contestar && millis() > tiempo + tiempoDelay ){
    serial.println(mensaje);
    Serial.println("Contesté: " + mensaje);
    digitalWrite(led, LOW);
    contestar = false;
  }

  //Router
  if (serial.available() > 0){
    String data = serial.readStringUntil('\n');
    if (data[1] == idMaquina){
      Serial.println(data);
      mensaje = data; mensaje[0] = idMaquina; mensaje[1] = idDestino;
      tiempo = millis();
      digitalWrite(led, HIGH);
      contestar = true;
    }else{
      Serial.println("Reenvié: " + data);
      serial.println(data);
    } 
  }
}
