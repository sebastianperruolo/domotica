#include <SoftwareSerial.h>

SoftwareSerial mySerial(10, 11); // RX, TX

char NODO='1';

void setup() {
  Serial.begin(9600);
//  while (!Serial) {
//    ; // wait for serial port to connect. Needed for Native USB only
//  }

  Serial.println("Serial inicializado");

  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);
  Serial.println("SoftwareSerial inicializado");

  pinMode(LED_BUILTIN, OUTPUT);

  mySerial.println("3BOLA");
}

void loop() {
  
  if (mySerial.available()) {
    String data = mySerial.readStringUntil('\n');
    Serial.println("Llegó " + data);
    
    char receptor = data[0];
    Serial.print("En 0:");
    Serial.println(receptor);

    if (receptor != '0' && receptor != '1' && receptor != '2' && receptor != '3') {
       Serial.println("Tiro la basura");
       return;
    }
    
    if (receptor == NODO) {
      Serial.println("Es para mi, prendo led");
      digitalWrite(LED_BUILTIN, HIGH);
      delay(1000);
      digitalWrite(LED_BUILTIN, LOW);
      data[0] = '3';
    }
    
    Serial.println("Enviando " + data);
    mySerial.println(data);
  }

}
