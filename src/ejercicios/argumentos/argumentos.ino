

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(13, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available()) {
    String data=Serial.readStringUntil('\n');
    Serial.print("LEI 0:");
    Serial.print(data[0]);
    Serial.print(" y 1:");
    Serial.println(data[1]);

    if (data[0]=='A') {
      delay(((int)data[1]-48)*1000);
      digitalWrite(13, 0);
      Serial.println("OFF");
    }

    if (data[0]=='E') {
      delay(((int)data[1]-48)*1000);
      digitalWrite(13, 1);
      Serial.println("ON");
    }
  }
}
