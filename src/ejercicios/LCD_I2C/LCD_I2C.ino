#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <DS1302.h>

LiquidCrystal_I2C lcd(0x3F,16,2);

DS1302 rtc(2, 3, 4);

void setup() {
  lcd.init();                      // initialize the lcd 
  //lcd.init();
  // Print a message to the LCD.
  lcd.backlight();
  lcd.setCursor(1,0);
  lcd.print("hello everyone");
  lcd.setCursor(1,1);
  lcd.print("konichiwaa");
  Serial.begin(9600);
}


void loop() {
  if (Serial.available()) {
    String leer = Serial.readStringUntil('\n');
    lcd.clear();
    lcd.setCursor(1,0);
    lcd.print(leer);
    Serial.println("Escribi: " + leer);
  }
  
}
