
#include "DoorbellSwitch.h"

DoorbellSwitch doorbellSwitch = DoorbellSwitch();
     
void setup() {
  Serial.begin(9600);
  Serial.println("SensorMovimiento setup");
  doorbellSwitch.setup();

  pinMode(LED_BUILTIN, OUTPUT);      // declare LED as output
  
}
     
void loop(){
  if (doorbellSwitch.pressDetected()) {
    Serial.println("Press detected");
  }
}
