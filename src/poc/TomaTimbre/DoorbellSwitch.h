#ifndef DOORBELL_SWITCH_H
#define DOORBELL_SWITCH_H

#include <Arduino.h>

const int DOORBELL_SWITCH_PIN = 0;

class DoorbellSwitch {
  private:
    int switchState;
    int offState;
  public:
    void setup();
    boolean pressDetected();
};

#endif
