#include "DoorbellSwitch.h"

void DoorbellSwitch::setup() {
  pinMode(DOORBELL_SWITCH_PIN, INPUT_PULLUP);
  switchState = digitalRead(DOORBELL_SWITCH_PIN);
  offState = switchState;
}

boolean DoorbellSwitch::pressDetected() {
  int val = digitalRead(DOORBELL_SWITCH_PIN);
  if (val != switchState) {
    switchState = val;
    if (switchState != offState) {
        return true;
    } else {
      Serial.println("DoorbellSwitch::Press stopped");
    }
  }
  
  return false;
}
