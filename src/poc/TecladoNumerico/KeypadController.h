#ifndef KEYPAD_CONTROLLER_H
#define KEYPAD_CONTROLLER_H

#include <Arduino.h>
#include <Keypad.h>

const char COMMAND_1 = '#';
const char COMMAND_2 = '*';

const byte ROWS = 4; // four rows
const byte COLS = 4; // four columns

class KeypadController {

  private:
    
    // define the symbols on the buttons of the keypads
    char calculatorKeys[ROWS][COLS] = {{'1', '2', '3', 'A'},
                                      {'4', '5', '6', 'B'},
                                      {'7', '8', '9', 'C'},
                                      {COMMAND_2, '0', COMMAND_1, 'D'}};

    // connect to the row pinouts of the keypad
    byte rowPins[ROWS] = {9, 8, 7, 6};
    // connect to the column pinouts of the keypad
    byte colPins[COLS] = {5, 4, 3, 2};
    Keypad myKeypad =
        Keypad(makeKeymap(calculatorKeys), rowPins, colPins, ROWS, COLS);

    String currentCode = "";

  public:
    boolean toggle();
    String getCode();
};

#endif
