#include "KeypadController.h"


boolean KeypadController::toggle() {
    char key = myKeypad.getKey();
    if (key == NO_KEY) {
      return false;
    }
    Serial.print(key);
    if (key == '#') {
      Serial.println(" code done");
      return true;
    }
    if (isDigit(key)) {
      currentCode += key;  
      Serial.print(" added. Now got ");
      Serial.println(currentCode);
      return false;
    }
    Serial.println(" ignored");
    return false;
}

String KeypadController::getCode() {
  String code = currentCode;
  currentCode = "";
  return code;
}
