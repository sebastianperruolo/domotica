/* 
    circuits4you.com
    4x4 keypad interfacing with arduino
*/
#include "KeypadController.h"
 
KeypadController keypadController = KeypadController();

void setup(){
  Serial.begin(9600);
  Serial.println("TecladoNumerico setup");
}
  
void loop(){

  if (keypadController.toggle()) {
    Serial.print("Código ingresado: ");
    Serial.println(keypadController.getCode());
  }

}
