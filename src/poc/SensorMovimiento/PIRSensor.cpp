#include "PIRSensor.h"



void PIRSensor::setup() {
  pinMode(SENSOR_PIN, INPUT);
  pirState = digitalRead(SENSOR_PIN);
  offState = pirState;
}

boolean PIRSensor::motionDetected() {
  int val = digitalRead(SENSOR_PIN);
  if (val != pirState) {
    pirState = val;
    if (pirState != offState) {
        return true;
    } else {
      Serial.println("PIRSensor::Motion stopped");
    }
  }
  
  return false;
}
