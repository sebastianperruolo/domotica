#ifndef PIR_SENSOR_H
#define PIR_SENSOR_H

#include <Arduino.h>

const int SENSOR_PIN = 10;

class PIRSensor {
  private:
    int pirState;
    int offState;
  public:
    void setup();
    boolean motionDetected();
};

#endif
