
#include "PIRSensor.h"

PIRSensor pirSensor = PIRSensor();
     
void setup() {
  Serial.begin(9600);
  Serial.println("SensorMovimiento setup");
  pirSensor.setup();

  pinMode(LED_BUILTIN, OUTPUT);      // declare LED as output
  
}
     
void loop(){
  if (pirSensor.motionDetected()) {
    Serial.println("Motion detected");
  }
}
