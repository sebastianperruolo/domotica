#ifndef WemosDigitalPinout_h
#define WemosDigitalPinout_h

static const int WEMOS_D1 = 5;
const int WEMOS_D2 = 4;
const int WEMOS_D3 = 0;
const int WEMOS_D4 = 2; // Mismo que LED_BUILTIN
const int WEMOS_D6 = 12;
const int WEMOS_D7 = 13;

#endif
