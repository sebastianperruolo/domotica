#include "Arduino.h"

#include "WemosDigitalPinout.h"

int PIN_0 = 99;
int NO_PIN = -1;

String PIN_UNKNOWN = "U";
String PIN_FAILS = "F";

void setup() {
  Serial.begin(9600);
  Serial.println("Setup");

  for (int i=0;i<16;i++) {
    String pinName = getWemosPinName(i);
    if (pinName == PIN_UNKNOWN) {
      logPin(i, "Unkonwn, want to try it?");
    } else if (pinName == PIN_FAILS) {
      logPin(i, "Unavailable");
    } else {
      logPin(i, pinName);
      pinOutput(i);
    }
  }

  logPin(PIN_0, "is " + getWemosPinName(PIN_0));

//  Serial.println("3, 6, 7, 8, 11 no funcionan");
}

void loop() {
  int newPin = serialReadInt();
  
  if (newPin != NO_PIN) {
    String pinName = getWemosPinName(newPin);

    if (pinName == PIN_UNKNOWN) {
      logPin(newPin, "Will try");
      pinOutput(newPin);
      pinToggle(newPin);
    } else if (pinName == PIN_FAILS) {
      logPin(newPin, "Will fail");
    } else {
      logPin(newPin, "is "+ pinName);
      pinToggle(newPin);
    }
  }
 
}

int serialReadInt() {
  int newPin = 0;
  if (Serial.available()) {
    newPin = Serial.parseInt();
  }
  //TODO solucionar falsos 0
  if (newPin == 0) {
    return NO_PIN;
  }

  if (newPin == PIN_0) {
    return 0;
  }

  return newPin;
}

String getWemosPinName(int pin) {
  if (pin == WEMOS_D1) {
    return "D1";
  } 
  if (pin == WEMOS_D2) {
    return "D2";
  }
  if (pin == WEMOS_D3) {
    return "D3";
  } 
  if (pin == WEMOS_D4) {
    return "D4";
  }
  if (pin == WEMOS_D6) {
    return "D6";
  }
  if (pin == WEMOS_D7) {
    return "D7";
  }
  if (pin == 1 || pin == 3 || pin == 6 || pin == 7||pin == 8||pin == 11) {
    return PIN_FAILS;
  }
  return PIN_UNKNOWN;
}

void logPin(int pin, String message) {
  Serial.print(pin);  
  Serial.print(": ");
  Serial.println(message);
}

void pinOutput(int pin) {
  logPin(pin, "pinMode OUTPUT");
  pinMode(pin, OUTPUT);
}

void pinToggle(int pin) {
  int newState = !digitalRead(pin);
  if (newState == HIGH) {
     logPin(pin, "digitalWrite HIGH");
     digitalWrite(pin, HIGH);
  } else {
    logPin(pin, "digitalWrite LOW");
    digitalWrite(pin, LOW);
  }
}
